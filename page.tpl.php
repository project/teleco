<?php // $Id$
/**
 * @file
 *  page.tpl.php
 *
 * Theme implementation to display a single Drupal page.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language; ?>" xml:lang="<?php print $language->language; ?>">
<head>
  <meta http-equiv="Content-Style-Type" content="text/css" />
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <?php print $styles; ?>
  <!--[if IE]>
    <link type="text/css" rel="stylesheet" media="all" href="<?php print $base_path . $directory; ?>/ie.css" >
  <![endif]-->
  <?php print $scripts; ?>
</head>
<body id="teleco" class="<?php print $body_classes; ?> <?php print $logo ? 'with-logo' : 'no-logo' ; ?>">

  <div id="skip-to-content">
    <a href="#main-content"><?php print t('Skip to main content'); ?></a>
  </div>

    <div id="page">

      <div id="header">
	    <div id="header-wrapper" class="clearfix">
		
          <div id="head-elements">
            <?php if ($logo): ?>
            <div id="logo">
              <a href="<?php print check_url($front_page) ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" /></a>
			  </div>
            <?php endif; ?>
          <?php if ($site_name): ?>
          <div id="site-name">
            <h1><a href="<?php print $base_path ?>" title="<?php print t('Home'); ?>"><?php print $site_name; ?></a></h1>
		  </div>	
          <?php endif; ?>
          <?php if ($site_slogan): ?>
            <div id="site-slogan"><em><?php print $site_slogan; ?></em></div>
          <?php endif; ?>
        </div> <!-- /#head-elements -->
		
          <?php if ($search_box): ?>
            <div id="search-box">
              <?php print $search_box; ?>
            </div> <!-- /#search-box -->
          <?php endif; ?>
      </div> <!-- /#header-wrapper -->
  
        <div id="header-bottom" class="clearfix">
       <?php if ($primary_menu): ?>
          <!-- Primary || Superfish -->
          <div id="primary">
            <div id="primary-inner">
                <?php print $primary_menu; ?>
            </div> <!-- /inner -->
          </div> <!-- /primary || superfish -->
        <?php endif; ?>
		
    </div> <!--/#header-bottom -->
    </div> <!--/#header -->
	  <div id="top-bar">
       <?php if ($mission): ?>
          <div id="mission">
          <?php print $mission; ?>
		  </div>
        <?php endif; ?>
         <?php if ($breadcrumb): ?>
            <?php print $breadcrumb; ?>
          <?php endif; ?>
	  </div>
      <div id="header-slide" class="region region-header">
      </div> <!-- /#header-slide -->
    <?php if ($header): ?>
      <div id="header-blocks" class="region region-header">
        <?php print $header; ?>
      </div> <!-- /#header-blocks -->
    <?php endif; ?>
        <?php if ($secondary_links) : ?>
		      <div id="secondary-links">
          <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')) ?>
      </div>
        <?php endif; ?>
      
    <div id="main" class="clear-block <?php print $header ? 'with-header-blocks' : 'no-header-blocks' ; ?>">

      <div id="content"><div id="content-inner">

        <?php if ($content_top): ?>
          <div id="content-top" class="region region-content_top">
            <?php print $content_top; ?>
          </div> <!-- /#content-top -->
        <?php endif; ?>

        <div id="content-header" class="clearfix">
          <?php if ($tabs): ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
          <a name="main-content" id="main-content"></a>
          <?php if ($title): ?><h1 class="title"><?php print $title; ?></h1><?php endif; ?>
          <?php if ($messages): print $messages; endif; ?>
          <?php if ($help): print $help; endif; ?>
        </div> <!-- /#content-header -->

        <div id="content-area">
          <?php print $content; ?>
        </div>

        <?php if ($content_bottom): ?>
          <div id="content-bottom" class="region region-content_bottom">
            <?php print $content_bottom; ?>
          </div> <!-- /#content-bottom -->
        <?php endif; ?>

        <?php if ($feed_icons): ?>
          <div class="feed-icons"><?php print $feed_icons; ?></div>
        <?php endif; ?>

      </div></div> <!-- /#content-inner, /#content -->

      <?php if ($left): ?>
        <div id="sidebar-left" class="region region-left">
          <?php print $left; ?>
        </div> <!-- /#sidebar-left -->
      <?php endif; ?>

      <?php if ($right): ?>
        <div id="sidebar-right" class="region region-right">
          <?php print $right; ?>
        </div> <!-- /#sidebar-right -->
      <?php endif; ?>

    </div> <!-- #main -->

    <div id="footer" class="region region-footer">
        <?php if ($secondary_links) : ?>
          <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')) ?>
        <?php endif; ?>
      <?php if ($footer): 
	  print $footer; endif; ?>
        <div id="footer-message">
          <?php print $footer_message; ?>
        </div> <!-- /#footer-message -->
    </div> <!-- /#footer -->

  </div> <!--/#page -->

  <?php if ($closure_region): ?>
    <div id="closure-blocks" class="region region-closure">
      <?php print $closure_region; ?>
    </div>
  <?php endif; ?>

  <?php print $closure; ?>

</body>
</html>