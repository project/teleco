<?php /* $Id$ */
 // Conditionally load the Superfish JS

/**
 * Override or insert PHPTemplate variables into the page templates.
 *
 * @param $vars
 *   A sequential array of variables to pass to the theme template.
 * @param $hook
 *   The name of the theme function being called ("page" in this case.)
 */
function teleco_preprocess_page(&$vars, $hook) {
  global $theme;

  $vars['primary_menu'] = menu_tree(variable_get('menu_primary_links_source', 'primary-links'));
  // Classes for body element. Allows advanced theming based on context
  // (home page, node of certain type, etc.)
  $body_classes = array($vars['body_classes']);
    if (!$vars['is_front']) {
      // Add unique classes for each page and website section
      $path = drupal_get_path_alias($_GET['q']);
      list($section, ) = explode('/', $path, 2);
      $body_classes[] = teleco_id_safe('page-'. $path);
      $body_classes[] = teleco_id_safe('section-'. $section);
      if (arg(0) == 'node') {
        if (arg(1) == 'add') {
          if ($section == 'node') {
            array_pop($body_classes); // Remove 'section-node'
          }
          $body_classes[] = 'section-node-add'; // Add 'section-node-add'
        }
        elseif (is_numeric(arg(1)) && (arg(2) == 'edit' || arg(2) == 'delete')) {
          if ($section == 'node') {
            array_pop($body_classes); // Remove 'section-node'
          }
          $body_classes[] = 'section-node-'. arg(2); // Add 'section-node-edit' or 'section-node-delete'
        }
      }
    // Add a unique class when viewing a node
    if (arg(0) == 'node' && is_numeric(arg(1))) {
      $body_classes[] = 'node-full-view'; // Add 'node-full-view'
    }
  }
  $vars['body_classes'] = implode(' ', $body_classes); // Concatenate with spaces
}

/**
 * Override or insert PHPTemplate variables into the block templates.
 *
 * @param $vars
 *   A sequential array of variables to pass to the theme template.
 * @param $hook
 *   The name of the theme function being called ("block" in this case.)
 */
function teleco_preprocess_block(&$vars, $hook) {
  $block = $vars['block'];

  // Special classes for blocks
  $block_classes = array();
  $block_classes[] = 'block-'. $block->module;
  $block_classes[] = 'region-'. $vars['block_zebra'];
  $block_classes[] = $vars['zebra'];
  $block_classes[] = 'region-count-'. $vars['block_id'];
  $block_classes[] = 'count-'. $vars['id'];
  $vars['block_classes'] = implode(' ', $block_classes);

}

/**
 * Converts a string to a suitable html ID attribute.
 *
 * - Preceeds initial numeric with 'n' character.
 * - Replaces space and underscore with dash.
 * - Converts entire string to lowercase.
 * - Works for classes too!
 *
 * @param string $string
 *   The string
 * @return
 *   The converted string
 */
function teleco_id_safe($string) {
  if (is_numeric($string{0})) {
    // If the first character is numeric, add 'n' in front
    $string = 'n'. $string;
  }
  return strtolower(preg_replace('/[^a-zA-Z0-9-]+/', '-', $string));
}










